job('CommonESBArtifacts') {
    scm {
        git {
            remote {
		        url('https://bitbucket.org/exeterdevelopers/commonesbartifacts.git')        
		    }
        }
    }
    triggers {
        scm('H/5 * * * *')
    }
    steps {
        maven('clean install')
        maven {
            goals('clean deploy -Dcarbon.url=https://esb-manager:9443 -Dcarbon.user=admin -Dcarbon.pass=admin')
            rootPOM("CommonESBArtifactsCAR/pom.xml")
        }
    }
}